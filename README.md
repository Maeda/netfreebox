# netfreebox
Script retournant (texte) les informations de la Freebox du FAI free.fr sous la forme : débit montant actuel/%utilisé - débit descendant actuel/%utilisé. Il indique également si la Freebox n'est plus joignable.

Nécessite iconv
Fonctionne sur Freebox V5 pas sur la POP.

Ne possédant plus de Freebox V5, ce script ne sera plus mis à jour.